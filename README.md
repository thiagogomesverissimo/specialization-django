# specializations-django

Exemplos:

- https://www.dj4e.com/
- https://github.com/csev/dj4e-samples
- https://help.pythonanywhere.com/pages/FollowingTheDjangoTutorial/
- https://docs.djangoproject.com/en/3.0/intro/tutorial01/

Instalação do django:

    sudo apt install virtualenvwrapper 
    mkvirtualenv django3 # --python=/usr/bin/python3.8
    source "/usr/local/bin/virtualenvwrapper.sh"
    workon django3
    pip install django
    python -m django --version

Se sair e voltar no terminal, reative o virtualenv

    workon django3
    pip3 install -r requirements.txt
    python manage.py check
    python manage.py makemigrations
    python manage.py migrate
    python manage.py runserver # local dev

Configuração deste curso:

1. django-admin startproject mysite
2. Em settings.py na variável ALLOWED_HOST deixar ['*']
3. Na interface do pythonanywhere, criar um novo web app, opção manual.
4. Colocar o endereço do Source code e Working directory o que
contém o arquivo manage.py, no meu caso: /home/thiagogomesverissimo/django_projects/mysite
5. arquivo wsgi.py:

    import os
    import sys

    path = os.path.expanduser('~/django_projects/mysite')
    if path not in sys.path:
        sys.path.insert(0, path)
    os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'
    from django.core.wsgi import get_wsgi_application
    from django.contrib.staticfiles.handlers import StaticFilesHandler
    application = StaticFilesHandler(get_wsgi_application())

6. Arrumar o virtualenv para: /home/thiagogomesverissimo/.virtualenvs/django3
7. Criar outro app:

    cd ~/django_projects/mysite
    python3 manage.py startapp polls

8. Em polls/views.py deixar apenas

    from django.http import HttpResponse
    def index(request):
        return HttpResponse("Hello, world. You're at the polls index.")
 
9. Em polls/url.py

    from django.urls import path
    from . import views

    urlpatterns = [
        path('', views.index, name='index'),
    ]

10. em mysite/urls.py

    from django.contrib import admin
    from django.urls import include, path

    urlpatterns = [
        path('polls/', include('polls.urls')),
        path('admin/', admin.site.urls),
    ]

11. rodar: python manage.py check que por trás faz: python manage.py runserver

12. Admin frontend

    python3 manage.py createsuperuser
    http://127.0.0.1:8000/admin/
