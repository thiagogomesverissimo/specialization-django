from django.shortcuts import render, redirect
from django.views import generic, View
from autos.models import Auto, Make
from autos.forms import AutoForm
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

# 
class AutoIndex(LoginRequiredMixin,generic.ListView):
    template_name = 'autos/index.html'
    context_object_name = 'autos'

    def get_queryset(self):
        return Auto.objects.all()

class AutoDetail(LoginRequiredMixin,generic.DetailView):
    template_name = 'autos/detail.html'
    model = Auto
    context_object_name = 'auto'

class AutoCreate(LoginRequiredMixin,View):
    template = 'autos/form.html'

    def get(self, request):       
        return render(request, self.template, {
            'form': AutoForm()
        })

    def post(self, request):
        form = AutoForm(request.POST)
        if not form.is_valid():
            return render(request, self.template, {
                'form': AutoForm()
            })

        auto = form.save()
        return redirect('/autos')

# um outro jeito de fazer que não implementando o método get e post e o Form
class AutoUpdate(LoginRequiredMixin,UpdateView):
    template_name = 'autos/form.html'
    model = Auto
    fields = '__all__'
    success_url = '/autos'

# LoginRequiredMixin, 
class AutoDelete(LoginRequiredMixin,DeleteView):
    template_name = 'autos/confirm_delete.html'
    model = Auto
    fields = '__all__'
    #success_url = reverse_lazy('autos:all')
    success_url = '/autos'

###############
class MakeIndex(LoginRequiredMixin,generic.ListView):
    template_name = 'makes/index.html'
    context_object_name = 'makes'

    def get_queryset(self):
        return Make.objects.all()

class MakeDetail(LoginRequiredMixin,generic.DetailView):
    template_name = 'makes/detail.html'
    model = Make
    context_object_name = 'make'

class MakeCreate(LoginRequiredMixin,CreateView):
    template_name = 'makes/form.html'
    model = Make
    fields = '__all__'
    success_url = '/autos/'

class MakeUpdate(LoginRequiredMixin,UpdateView):
    template_name = 'makes/form.html'
    model = Make
    fields = '__all__'
    success_url = '/autos/'

# LoginRequiredMixin, 
class MakeDelete(LoginRequiredMixin,DeleteView):
    template_name = 'makes/confirm_delete.html'
    model = Make
    fields = '__all__'
    #success_url = reverse_lazy('autos:all')
    success_url = '/autos/'