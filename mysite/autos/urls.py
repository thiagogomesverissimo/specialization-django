from django.urls import path
from . import views

app_name = 'autos'
urlpatterns = [
    # index
    path('',views.AutoIndex.as_view()),

    # show
    path('main/<int:pk>/',views.AutoDetail.as_view()),

    # create and store
    path('main/create/', views.AutoCreate.as_view()),
    
    # edit and update
    path('main/<int:pk>/update/', views.AutoUpdate.as_view()),

    # delete
    path('main/<int:pk>/delete/', views.AutoDelete.as_view()),
    
    # lookup
    path('lookup/',views.MakeIndex.as_view()),
    path('lookup/<int:pk>/',views.MakeDetail.as_view()),
    path('lookup/create/', views.MakeCreate.as_view()),
    path('lookup/<int:pk>/update/', views.MakeUpdate.as_view()),
    path('lookup/<int:pk>/delete/', views.MakeDelete.as_view()),
]
