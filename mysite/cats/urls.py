from django.urls import path
from . import views

app_name = 'cats'
urlpatterns = [
    # index
    path('',views.CatIndex.as_view()),
    path('main/<int:pk>/',views.CatDetail.as_view()),
    path('main/create/', views.CatCreate.as_view()),
    path('main/<int:pk>/update/', views.CatUpdate.as_view()),
    path('main/<int:pk>/delete/', views.CatDelete.as_view()),
    
    # lookup
    path('lookup/',views.BreedIndex.as_view()),
    path('lookup/<int:pk>/',views.BreedDetail.as_view()),
    path('lookup/create/', views.BreedCreate.as_view()),
    path('lookup/<int:pk>/update/', views.BreedUpdate.as_view()),
    path('lookup/<int:pk>/delete/', views.BreedDelete.as_view()),
]
