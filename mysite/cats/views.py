from django.shortcuts import render, redirect
from django.views import generic, View
from cats.models import Cat, Breed
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

# 
class CatIndex(LoginRequiredMixin,generic.ListView):
    template_name = 'cats/index.html'
    context_object_name = 'cats'

    def get_queryset(self):
        return Cat.objects.all()

class CatDetail(LoginRequiredMixin,generic.DetailView):
    template_name = 'cats/detail.html'
    model = Cat
    context_object_name = 'cat'

class CatCreate(LoginRequiredMixin,CreateView):
    template_name = 'cats/form.html'
    model = Cat
    fields = '__all__'
    success_url = '/cats/'

class CatUpdate(LoginRequiredMixin,UpdateView):
    template_name = 'cats/form.html'
    model = Cat
    fields = '__all__'
    success_url = '/cats'

# LoginRequiredMixin, 
class CatDelete(LoginRequiredMixin,DeleteView):
    template_name = 'cats/confirm_delete.html'
    model = Cat
    fields = '__all__'
    #success_url = reverse_lazy('cats:all')
    success_url = '/cats'

###############
class BreedIndex(LoginRequiredMixin,generic.ListView):
    template_name = 'breeds/index.html'
    context_object_name = 'breeds'

    def get_queryset(self):
        return Breed.objects.all()

class BreedDetail(LoginRequiredMixin,generic.DetailView):
    template_name = 'breeds/detail.html'
    model = Breed
    context_object_name = 'breed'

class BreedCreate(LoginRequiredMixin,CreateView):
    template_name = 'breeds/form.html'
    model = Breed
    fields = '__all__'
    success_url = '/cats/'

class BreedUpdate(LoginRequiredMixin,UpdateView):
    template_name = 'breeds/form.html'
    model = Breed
    fields = '__all__'
    success_url = '/cats/'

# LoginRequiredMixin, 
class BreedDelete(LoginRequiredMixin,DeleteView):
    template_name = 'breeds/confirm_delete.html'
    model = Breed
    fields = '__all__'
    #success_url = reverse_lazy('cats:all')
    success_url = '/cats/'
