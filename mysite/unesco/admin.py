from django.contrib import admin

# Register your models here.

from unesco.models import Site, Iso, Category, State,Region

admin.site.register(Site)
admin.site.register(Region)
admin.site.register(Category)
admin.site.register(Iso)
admin.site.register(State)
