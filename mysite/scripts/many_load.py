import csv  # https://docs.python.org/3/library/csv.html
from unesco.models import Site, Iso, Category, State,Region

# https://django-extensions.readthedocs.io/en/latest/runscript.html

# python3 manage.py runscript many_load
# name,description,justification,year,longitude,latitude,area_hectares,category,states,region,iso

def run():
    fhand = open('unesco/data.csv')
    reader = csv.reader(fhand)
    next(reader)  # Advance past the header

    Iso.objects.all().delete()
    Category.objects.all().delete()
    Region.objects.all().delete()
    State.objects.all().delete()
    Site.objects.all().delete()

    for row in reader:
        name = row[0]
        description = row[1]
        justification = row[2]

        try:
            year = int(row[3])
        except:
            year = None
    
        try:
            longitude = float(row[4])
        except:
            longitude = None

        try:
            latitude = float(row[5])
        except:
            latitude = None

        try:
            area_hectares = float(row[6])
        except:
            area_hectares = None

        category = row[7]
        state = row[8]
        region = row[9]
        iso = row[10]

        category_obj, created = Category.objects.get_or_create(name=category)
        iso_obj, created = Iso.objects.get_or_create(name=iso)
        region_obj, created = Region.objects.get_or_create(name=region)
        state_obj, created = State.objects.get_or_create(name=state)

        #print('------------------------------------')
        #print(row)
        #print('------------------------------------')

        site = Site(
            name = name,
            description = description,
            justification = justification,
            year = year,
            longitude = longitude,
            latitude = latitude,
            area_hectares = area_hectares,
            category = category_obj,
            state = state_obj,
            region = region_obj,
            iso = iso_obj  
        )
        site.save()
