from django.shortcuts import render
from django.http import HttpResponse

def index(request):

    visitas = request.session.get('visitas',0) + 1
    request.session['visitas'] = visitas
    if visitas > 4:
        del(request.session['visitas'])
    response = render(request,'hello/index.html',{
        'visitas': visitas,
    })   
    response.set_cookie('dj4e_cookie', '3937230d', max_age=1000)
    return response
