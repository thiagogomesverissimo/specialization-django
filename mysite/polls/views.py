from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from polls.models import Question, Choice
from django.views import generic

# Inserindo uns registros manualmente:
# python3 manage.py shell
# from django.utils import timezone
# from polls.models import Question, Choice
# q = Question(question_text="qual sua cor preferida?", pub_date=timezone.now())
# q.save()
# Question.objects.all()
# Question.objects.get(pk=1)

# q.choice_set.create(choice_text='azul',votes=0)
# q.choice_set.create(choice_text='vermelho',votes=0)
# q.choice_set.all()
# Choice.objects.all() 

#def index(request):
#    last5 = Question.objects.order_by('-pub_date')[:5]
#    context = {'questions':last5}
#    return render(request,'polls/index.html',context)
class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'questions'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')[:5]


#def detail(request, question_id):
#    question = get_object_or_404(Question, pk=question_id)
#    return render(request,'polls/detail.html',{'question':question})
class DetailView(generic.DetailView):
    template_name = 'polls/detail.html'
    model = Question

#def results(request, question_id):
#    question = get_object_or_404(Question, pk=question_id)
#    return render(request,'polls/results.html',{'question': question})
class ResultView(generic.DetailView):
    template_name = 'polls/results.html'
    model = Question

def vote(request, question_id):
    # uma espécie de validação
    question = get_object_or_404(Question, pk=question_id)
    try:
        select_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request,'polls/detail.html',{
            'error_message': "You didn't select a choice ",
            'question': question
        })
    select_choice.votes += 1
    select_choice.save()
    return HttpResponseRedirect("/polls/" + str(question.id) + "/results")

def owner(request):
   return HttpResponse("Hello, world. 3937230d  is the polls owner.")

