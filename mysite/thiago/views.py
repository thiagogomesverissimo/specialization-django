from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, logout

from thiago.forms import BasicForm

def login(request):
    return render(request,'thiago/login.html')

def login2(request):
    user = authenticate(
        username=request.POST.get('username'), 
        password=request.POST.get('password')
    )
    return redirect('/thiago/login')

def logout_view(request):
    logout(request)
    return redirect('/thiago/login')

def form_basico(request) :
    form = BasicForm()
    return HttpResponse(form.as_table())

# if not request.user.is_authenticated :
#            loginurl = reverse('login')+'?'+urlencode({'next': request.path})
            #return redirect(loginurl)