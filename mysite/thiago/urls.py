from django.urls import path
from . import views

app_name = 'thiago'

urlpatterns = [
    # os dois modos, com generic views ou diretão
    path('login', views.login),
    path('login2', views.login2),
    path('logout', views.logout),
    path('form_basico', views.form_basico),
    
    #path('',views.IndexView.as_view(), name="index"),

    #path('<int:question_id>',views.detail, name="detail"),
    #path('<int:pk>', views.DetailView.as_view(), name="detail"),

    #path('<int:question_id>/results/',views.results, name="results"),
    #path('<int:pk>/results',views.ResultView.as_view(),name="results"),

    #path('<int:question_id>/vote/',views.vote, name="vote"),

    #path('owner', views.owner, name='owner'),
]
